from flask import render_template, redirect
from flask import flash, get_flashed_messages
from WebApp import app, db
from WebApp.forms import SignUpForm
from WebApp.models import User

@app.route('/')
def home():
    return redirect('/signup')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignUpForm()
    afterForm = None
    if form.validate_on_submit():
        if form.birth_date.data is None:
            create_user = User(
                mobile_number = form.mobile_number.data,
                first_name = form.first_name.data,
                last_name = form.last_name.data,
                gender = form.gender.data,
                email_address = form.email_address.data,
            )
            db.session.add(create_user)
            db.session.commit()
            flash(
                "Registration is Complete, Please Go To Login Page",
                category="info"
            )
            afterForm = 'OK'
        else:
            create_user = User(
                mobile_number = form.mobile_number.data,
                first_name = form.first_name.data,
                last_name = form.last_name.data,
                birth_date = form.birth_date.data.strftime("%Y-%m-%d"),
                gender = form.gender.data,
                email_address = form.email_address.data,
            )
            db.session.add(create_user)
            db.session.commit()
            flash(
                "Registration is Complete, Please Go To Login Page",
                category="info"
            )
            afterForm = 'OK'
    if form.errors != {}:
        for err_msg in form.errors.values():
            flash(
                f"There was an error when creating the user: {err_msg}",
                category="danger"
            )
            afterForm = 'Failed'
    return render_template('signup.html', form=form, afterForm=afterForm)

@app.route('/signin', methods=['GET'])
def signin():
    return render_template('signin.html')