from flask_wtf import FlaskForm
from wtforms import StringField, DateField, RadioField, SubmitField
from wtforms.validators import Length, EqualTo, Email, DataRequired, Optional, ValidationError
from WebApp.models import User

class SignUpForm(FlaskForm):
    def validate_mobile_number(self, check_number):
        mobile_number = User.query.filter_by(
            mobile_number=check_number.data
        ).first()
        if mobile_number:
            raise ValidationError(
                "Mobile Number already exists! Please try a different Mobile Number"
            )
        if not check_number.data.isdigit():
            raise ValidationError(
                "Mobile Number is not digit! Please input the right one"
            )

    def validate_email_address(self, check_email):
        email_address = User.query.filter_by(
            email_address=check_email.data
        ).first()
        if email_address:
            raise ValidationError(
                "Email Address already exists! Please try a different Email Address"
            )

    ##########################################

    mobile_number = StringField(
        label="Number", validators=[Length(min=10, max=13), DataRequired()]
    )

    first_name = StringField(
        label="First Name", validators=[Length(min=1, max=30), DataRequired()]
    )

    last_name = StringField(
        label="Last Name", validators=[Length(min=1, max=30), DataRequired()]
    )

    birth_date = DateField("Date of Birth", validators=[Optional()])

    gender = RadioField(
        label="Gender", choices=["Male", "Female"], validators=[Optional()]
    )

    email_address = StringField(
        label="Email", validators=[Email(), DataRequired()]
    )

    submit = SubmitField(label="SignUp")

