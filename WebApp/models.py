from WebApp import db

class User(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    mobile_number = db.Column(db.String(), unique=True)
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    birth_date = db.Column(db.String(), nullable=True)
    gender = db.Column(db.String(), nullable=True)
    email_address = db.Column(db.String(), unique=True)